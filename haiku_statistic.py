#!/usr/bin/python
# -*- coding: utf-8 -*-

import external_modules
import settings
import time
from collections import OrderedDict
import json
from time import sleep
from datetime import datetime, timedelta, date


#====================== Freelist ======================
#title = "haiku_dev_ml"
#url_pre = "http://www.freelists.org/archive/haiku-development/" #only http!
#search_string = "/post/"
#data = external_modules.freelist_main_loop(url_pre, title, search_string)
#data_to_write = external_modules.sorter(data)
#external_modules.write_to_json(title, data_to_write)

#====================== Freelist ======================
#title = "haiku_general_ml"
#url_pre = "http://www.freelists.org/archive/haiku/" #only http!
#search_string = "/post/"
#data = external_modules.freelist_main_loop(url_pre, title, search_string)
#data_to_write = external_modules.sorter(data)
#external_modules.write_to_json(title, data_to_write)

#====================== Echelog ======================
title = "haiku_main_irc"
url_pre = "http://echelog.com/logs/browse/haiku/" #only http!
search_string = '<span class="d">&lt;'
haiku_main_irc_eredmeny = {}
timestamps = external_modules.date_generator_for_echelog()

daily_results = {}
for single_date, timestamp in timestamps.iteritems():
	url_pos = timestamp
	date = single_date
	current_return = external_modules.downloader( url_pre, url_pos, search_string )
	daily_results.update( { single_date : current_return } )
	sleep(1)

sorted_daily_results = OrderedDict(sorted(daily_results.items(), key=lambda x: x[0]))

date_temp = []
monthly_result_temp = 0
monthly_results = {}

for single_date, result in sorted_daily_results.iteritems():
	if not date_temp :
		date_temp = single_date[:7]
		monthly_result_temp = monthly_result_temp + result
	else :
		if (date_temp == single_date[:7] ) :
			monthly_result_temp = monthly_result_temp + result
		else :
			monthly_results.update( { date_temp : monthly_result_temp } )
			date_temp = []
			monthly_result_temp = 0
			date_temp = single_date[:7]
			monthly_result_temp = monthly_result_temp + result
	monthly_results.update( { date_temp : monthly_result_temp } )

sorted_monthly_results = OrderedDict(sorted(monthly_results.items(), key=lambda x: x[0]))

with open(title + '.json', 'w') as f:
	json.dump(sorted_monthly_results, f)



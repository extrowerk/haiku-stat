#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import urllib2
import cookielib
import shutil
import sys
from cookielib import CookieJar
from datetime import datetime, timedelta, date
from time import sleep
from collections import OrderedDict
import json
import settings

JUST_UPDATE = settings.JUST_UPDATE
UPDATE_INTERVAL = settings.UPDATE_INTERVAL
FIRST_POST_YEAR = settings.FIRST_POST_YEAR
FIRST_POST_MONTH = settings.FIRST_POST_MONTH

current_year = int(datetime.now().year)
current_month = int(datetime.now().month)
current_day = int(datetime.now().day)

if JUST_UPDATE==1 :
# Kezdo ev es honap meghatarozasa.
	if current_month > UPDATE_INTERVAL :
		start_year = current_year
		start_month = current_month-UPDATE_INTERVAL
	else:
		start_year = current_year-1
		start_month = 12+(current_month-UPDATE_INTERVAL)
else:
	start_year = FIRST_POST_YEAR
	start_month = FIRST_POST_MONTH


#Common functions

def getUrlAndCount(url, search_string):
	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

	HTMLSource = ""
	req = urllib2.Request(url,headers=hdr)
	cj = CookieJar()
	opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
	try :
		response = opener.open(req)
		page_data = response.read()
	except urllib2.HTTPError, e:
		page_data = ""
	talalat = page_data.count(search_string)
	print  "Találatok száma : %s" % (talalat)
	return talalat

def downloader (url_pre, url_pos, search_string):
	url = (url_pre + url_pos)
	talalat = getUrlAndCount(url, search_string)
	print "Itt tartunk: %s" % (url)
	return talalat

def sorter (data):
	data_to_write = OrderedDict(sorted(data.items(), key=lambda x: x[0]))
	return data_to_write

def write_to_json(title, data_to_write):
	with open(title + '.json', 'w') as f:
		json.dump(data_to_write, f)

# Echelog functions

start_day = 1
start_date_echelog = date(int(start_year), int(start_month), int(start_day))
end_day = 1
end_date_echelog = date(int(current_year), int(current_month), int(end_day))


def date_generator_for_echelog ():
	timestamps = {}
	timestamps_temp = {}
	for single_date in daterange(start_date_echelog, end_date_echelog):
		#timestamps.append(datetime.strptime(str(single_date), '%Y-%m-%d').strftime("%s"))
		#print single_date
		timestamps_temp = {str(single_date): datetime.strptime(str(single_date), '%Y-%m-%d').strftime("%s")}
		timestamps.update(timestamps_temp)
	#print timestamps
	return timestamps

def daterange(start_date_echelog, end_date_echelog): 
	#for date_generator_for_echelog
	for n in range(int ((end_date_echelog - start_date_echelog).days)):
		yield start_date_echelog + timedelta(n)

#Freelist functions

def freelist_generator():

	# Datumgeneralas. Formatum YYYY-MM
	date_for_url = []
	for year in range (start_year, current_year+1) :
		for month in range (1, 13):
			if (year==start_year and month<start_month) or (year==current_year and month>current_month-1) :
				continue
			date_for_url.append(str(month).zfill(2) + "-" + str(year))
	return date_for_url

def freelist_main_loop(url_pre, title, search_string):
	result = {}
	date_for_url = freelist_generator()

	for url_pos in date_for_url :
		current_return = downloader( url_pre, url_pos, search_string )
		date=url_pos[3:]+"-"+url_pos[:2]
		result.update( { date : current_return } )
		sleep(1)
	return result

